#!/bin/bash

for file in $(ls /etc/avahi/services); do
	sudo rm /etc/avahi/services/$file
done

if [ -f /usr/lib/x86_64-linux-gnu/avahi/service-types.db ]; then
	sudo rm /usr/lib/x86_64-linux-gnu/avahi/service-types.db
fi


