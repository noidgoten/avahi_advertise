#!/bin/bash

wget https://raw.githubusercontent.com/Distrotech/avahi/distrotech-avahi/service-type-database/build-db.in
wget https://raw.githubusercontent.com/Distrotech/avahi/distrotech-avahi/service-type-database/service-types

while read line; do
	echo >> service-types $line
done <names.txt	

for file in $(ls); do
	if [[ ${file: -8} == ".service" ]]; then
		sudo cp $file /etc/avahi/services
	fi
done

for file in $(ls /etc/avahi/services); do
	sudo chmod 774 /etc/avahi/services/$file
done

sed -e 's,@PYTHON\@,/usr/bin/python2.7,g' \
    -e 's,@DBM\@,gdbm,g' < build-db.in > build-db
/usr/bin/python2.7 build-db

rm build*
sudo mv service-types.db /usr/lib/x86_64-linux-gnu/avahi/service-types.db
rm service*
sudo service avahi-daemon restart

