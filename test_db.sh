if command -v gdbmtool >/dev/null ; then
	echo 'gdbmtool is already installed';
else
	echo 'installing gdbmtool'
	wget ftp://ftp.gnu.org/gnu/gdbm/gdbm-1.11.tar.gz
	tar -xzf gdbm-1.11.tar.gz
	rm gdbm-1.11.tar.gz
	cd gdbm-1.11
	bash ./configure --prefix=/usr --disable-static --enable-libgdbm-compat
	make
	make check
	sudo make install
	cd ..
	rm -r gdbm-1.11
fi

gdbmtool /usr/lib/x86_64-linux-gnu/avahi/service-types.db
