Use $ avahi-browse -art to scan for current services on the local network. 


#########


script.sh will install all .service files (inside this folder) so that avahi advertises them.

To add your own, use qotd.service as reference. To find protocol and port: <a href="http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml">IANA</a>.

Configure names.txt accordingly (type: name). Effect will only be local.

However, some already exist, like \_workstation.\_tcp, \_ssh.\_tcp.

Re-adding such names might cause problems (untested).


#########


Run test_db.sh to check your current names db. (needs gdbmtool, 1.11 will be installed if necessary)

Use > fetch _ssh._tcp, for instance.

Finally, > quit.


#########


To remove the current configuration use reset.sh.

